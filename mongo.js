/*
db.users.insertOne({

    "username": "cstrife",
    "password": "LimitBreakOmnislash"

})
*/

/*
db.users.insertOne({

    "username": "tlockhart",
    "password": "LimitBreakFinalHeaven"

})
*/


// Insert Multiple Documents at once

/*
db.users.insertMany(

    [
        {
            "username": "pablo123",
            "password": "123paul"
        },
        {
            "username": "pedro99",
            "password": "iampeter99"
        }
    ]
    
)
*/


/*
db.products.insertMany(
    [
        {
            "name": "Final Fantasy VII",
            "description": "Nintendo Switch Game from Square-Enix",
            "price": 70
        },
        {
            "name": "Final Fantasy VIII",
            "description": "Nintendo Switch Game from Square-Enix",
            "price": 70
        },
        {
            "name": "Final Fantasy IX",
            "description": "Nintendo Switch Game from Square-Enix",
            "price": 70
        }
    ]
)
*/

// Read / Retrieve
// db.collectionName.find() - return / find all documents in the collection

//db.users.find()


// db.collectionName.find({"criteria": "value"}) - rturns / find all documents that match the criteria
//db.users.find({"username": "pedro99"})

/*
db.cars.insertMany(
    [
        {
            "name": "Vios",
            "brand": "Toyota",
            "type": "Sedan",
            "price": 1500000
        },
        {
            "name": "Tamaraw FX",
            "brand": "Toyota",
            "type": "AUV",
            "price": 750000
        },
        {
            "name": "City",
            "brand": "Honda",
            "type": "Sedan",
            "price": 1600000
        },
    ]
)
*/

//db.cars.find({"type": "Sedan"})
//db.cars.find({"brand": "Toyota"})



// db.collectionName.findOne({}) - find / return the first item / document in the collection
//db.cars.findOne({})


// db.collectionName.findOne({"criteria": "value"}) - find / return the first item / document that matches the criteria
//db.cars.findOne({"type": "Sedan"})
//db.cars.findOne({"brand": "Toyota"})
//db.cars.findOne({"brand": "Honda"})


// Update

// db.collectionName.updateOne({"criteria": "value"},{$set:{"fieldToBeUpdated": "updatedValue"}})
// allows us to update the first item that matches our criteria
//db.users.updateOne({"username": "pedro99"},{$set:{"username": "peter1999"}})

// db.collectionName.updateOne({},{$set:{"fieldToBeUpdated": "updatedValue"}})
// allows us to update the first item in the collection
//db.users.updateOne({},{$set:{"username": "updatedUsername"}})


// db.collectionName.updateOne({"criteria": "value"},{$set:{"fieldToBeUpdatedThatIsNotExisting": "updatedValue"}})
// mongoDC will add the field into the document
//db.users.updateOne({"username": "pablo123"},{$set:{"isAdmin": true}})


// db.collectionName.updateOne({},{$set:{"fieldToBeUpdatedThatIsNotExisting": "updatedValue"}})
// mongoDC will add the field into the document for all (all items)
//db.users.updateMany({},{$set:{"isAdmin": true}})



// db.collectionName.updateMany({"criteria": "value"},{$set:{"fieldToBeUpdated": "updatedValue"}})
// allows us to update all items that matches our criteria
//db.cars.updateMany({"type": "Sedan"},{$set:{"price": 1000000}})


// Delete

// db.collectionName.deleteOne({}) - deletes first item in collection
//db.products.deleteOne({})

// db.collectionName.deleteOne({}) - deletes first item in collection that matches the criteria
//db.cars.deleteOne({"brand": "Toyota"})


// db.collectionName.deleteMany(Many{"criteria": "value"}) - deletes all items in collection that matches the criteria
//db.users.deleteMany({"isAdmin": true})




//db.collectionName.deleteMany({}) - deletes all documents in a collection
//db.products.deleteMany({})

//db.cars.deleteMany({})




